/*
    Create a Table
*/

#include <iostream>
#include <sqlite3.h>
#include <stdlib.h>

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    for(int i = 0; i < argc; i++)
    {
        std::cout << azColName[i] << " = " << argv[i] ? argv[i] : "NULL";
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return 0;
}

int main(int argc, char *argv[])
{
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    char *sql;

    /* Open database */
    rc = sqlite3_open("test.db", &db);
    if(rc)
    {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
    }
    else
    {
        std::cout << "Opened database successfully" << std::endl;
    }

    /* Create SQL statement */
    sql = "CREATE TABLE COMPANY("\
        "ID         INT PRIMARY KEY NOT NULL,"\
        "NAME       TEXT            NOT NULL,"\
        "AGE        INT             NOT NULL,"\
        "ADDRESS    CHAR(50),"\
        "SALARY     REAL);";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        std::cout << "SQL error: " << zErrMsg << std::endl;
        sqlite3_free(zErrMsg);
    }
    else
    {
        std::cout << "Table created successfully" << std::endl;
    }

    sqlite3_close(db);
    return 0;
}