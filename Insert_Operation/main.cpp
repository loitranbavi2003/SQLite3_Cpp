/*
    INSERT Operation
*/

#include <iostream>
#include <sqlite3.h>
#include <stdlib.h>

static int callback(void *NotUsed, int argc, char **argv, char **azColName) 
{
    // This function is not used in this case, but can be used to process data from queries
    for (int i = 0; i < argc; i++) 
    {
        std::cout << azColName[i] << " = " << argv[i] ? argv[i] : "NULL";
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return 0;
}

int main(int argc, char *argv[]) 
{
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    char *sql;

    /* Open database */
    rc = sqlite3_open("test.db", &db);
    if (rc) 
    {
        std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
        return 0; // Indicate error
    } 
    else 
    {
        std::cout << "Opened database successfully" << std::endl;
    }

    /* Create COMPANY table (if it doesn't exist) */
    sql = "CREATE TABLE IF NOT EXISTS COMPANY ( " \
          "ID INT PRIMARY KEY     NOT NULL, " \
          "NAME           TEXT    NOT NULL, " \
          "AGE            INT     NOT NULL, " \
          "ADDRESS        CHAR(50), " \
          "SALARY         REAL );";

    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if (rc != SQLITE_OK) 
    {
        std::cerr << "SQL error creating table: " << zErrMsg << std::endl;
        sqlite3_free(zErrMsg);
        sqlite3_close(db);
        return 0; // Indicate error
    }

    /* Create SQL statement for insertion */
    sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
          "VALUES (1, 'Paul', 32, 'California', 20000.00 ); " \
          "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "  \
          "VALUES (2, 'Allen', 25, 'Texas', 15000.00 ); "     \
          "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
          "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );" \
          "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)" \
          "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";

    /* Execute SQL statement for insertion */
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if (rc != SQLITE_OK) 
    {
        std::cout << "SQL error: " << zErrMsg << std::endl;
        sqlite3_free(zErrMsg);
    } 
    else 
    {
        std::cout << "Data inserted successfully" << std::endl;
    }

    sqlite3_close(db);
    return 0;
}
